import React from 'react'
import s from './HeaderBlock.module.scss';
import ReactLogoPng from '../../logo.png'
import { ReactComponent as ReactLogoSvg } from '../../logo.svg';

const HeaderBlock = () => {
    return (
        <div className={s.cover}>
            <div className={s.wrap}>
                <h1 className={s.header}>Learn words online</h1>
                <img src={ReactLogoPng}/>
                <ReactLogoSvg/>
                <p className={s.descr}>Use cards for learning and improve your vocabulary!</p>
            </div>
        </div>
    );
}

export default HeaderBlock;