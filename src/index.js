import React from 'react';
import ReactDom from 'react-dom';
import './index.css'
import HeaderBlock from './components/HeaderBlock';

const AppHeader = () => {
    return <h1 className={'header'}>Hello, React!!!</h1>
}

const AppList = () => {
    const items = ['Item 1', 'Item 2', 'Item 3']
    return (
        <ul>
            { items.map(item => <li>{item}</li>) }
            <li>{ items[0] }</li>
            <li>{ items[1] }</li>
        </ul>
    );
}

const App = () => {
    return (
        <React.Fragment>
            <HeaderBlock/>
            <AppHeader/>
            <AppList/>
        </React.Fragment>
    );
}

ReactDom.render(<App/>, document.getElementById('root'));